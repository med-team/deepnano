#!/bin/sh -e

pkg="deepnano"
test_required_pkg="${pkg}-data"
datadir=/usr/share/poretools/data
# Fail earlier than 2:17 in case #921566 still applies; each test has this
# amount of seconds to run.
timeout=600

if [ "$AUTOPKGTEST_TMP" = "" ] ; then
  AUTOPKGTEST_TMP=$(mktemp -d /tmp/${pkg}-test.XXXXXX)
  trap "rm -rf $AUTOPKGTEST_TMP" 0 INT QUIT ABRT PIPE TERM
fi

HOME="$AUTOPKGTEST_TMP"
export HOME
cp -a /usr/share/${test_required_pkg}/* $AUTOPKGTEST_TMP
mkdir -p $AUTOPKGTEST_TMP/test_data/

# Copy only 10 data sets since test with all data takes to long
for d in `ls $datadir | sort | head -n10` ; do cp $datadir/$d $AUTOPKGTEST_TMP/test_data/ ; done

cd $AUTOPKGTEST_TMP

find . -name "*gz" -exec gunzip \{\} \;

# Cap parallelization level: may help avoid #921566.
if [ "$(nproc)" -lt 16 ]
then OMP_NUM_THREADS="$(nproc)"
else OMP_NUM_THREADS=16
fi
export OMP_NUM_THREADS

echo "\n#1 - deepnano_basecall"
timeout --verbose "$timeout" deepnano_basecall test_data/*
cat output.fasta

echo "\n#2 - deepnano_basecall_no_metrichor"
timeout --verbose "$timeout" deepnano_basecall_no_metrichor test_data/*

echo "PASS"
